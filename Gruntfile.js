(function() {
	'use strict';

	module.exports = function(grunt) {
		// Sets up tasks and options for these task
		grunt.initConfig({
			pkg: grunt.file.readJSON('package.json'),
			sass: {
				dist: {
					files: {
						'assets/styles/css/style.css' : 'assets/styles/sass/main.scss'
					}
				}
			},
			watch: {
				css: {
					files: '**/*.scss',
					tasks: ['sass']
				},
				scripts: {
					files: ['./app/**/*.js', '!./app/app.min.js'],
					tasks: ['concat', 'uglify']
				}
			},
			bowerInstall: {
				target: {
					// Point to files that should be updated when running 'grunt bower-install'
					src: [
						'index.html'
					],
					// Optional
					cwd: '',
					dependencies: true,
					devDependencies: false,
					exclude: [],
					fileTypes: {},
					ignorePath: '',
					overrides: {}
				}
			},
			concat: {
				js: { //target
					src: [
						'./bower_components/angular/angular.js',
						'./bower_components/angular-ui-router/release/angular-ui-router.js',
						'./bower_components/ui-select/dist/select.js',
						'./bower_components/angular-sanitize/angular-sanitize.js',
						'./bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
						'./bower_components/momentjs/moment.js',
						'./bower_components/humanize-duration/humanize-duration.js',
						'./bower_components/angular-timer/dist/angular-timer.js',
						'./bower_components/SHA-1/sha1.js',
						'./bower_components/angulartics/src/angulartics.js','./bower_components/angulartics-google-analytics/lib/angulartics-google-analytics.js',
						'./bower_components/angular-cookie-law/dist/angular-cookie-law.min.js',

						'./app/app.module.js',
						'./app/app.constant.js',
						'./app/app.routes.js',
						'./app/shared/*.js',
						'./app/filters/*.js',
						'./app/about/*.js',
						'./app/faq/*.js',
						'./app/post/*.js',
						'./app/posts/*.js',
						'./app/global/*.js'
					],
					dest: './app/app.min.js'
				}
			},
			uglify: {
				js: { //target
					src: ['./app/app.min.js'],
					dest: './app/app.min.js'
				},
				options: {
					mangle: false
				}
			},
			processhtml: {
				dist: {
					files: {
						'./deploy/index.html': ['index.html']
					}
				}
			},
			copy: {
				main: {
					files: [
						{
							expand: true,
							flatten: true,
							src: ['./app/templates/*'],
							dest: './deploy/app/templates/',
							filter: 'isFile'
						},
						{
							expand: true,
							flatten: true,
							src: ['./app/app.min.js'],
							dest: './deploy/app/',
							filter: 'isFile'
						},
						{
							expand: true,
							flatten: true,
							src: ['./assets/images/*'],
							dest: './deploy/assets/images/',
							filter: 'isFile'
						},
						{
							expand: true,
							flatten: true,
							src: ['./assets/styles/css/*'],
							dest: './deploy/assets/styles/css/',
							filter: 'isFile'
						},
						{
							expand: true,
							flatten: true,
							src: ['./assets/scripts/*'],
							dest: './deploy/assets/scripts/',
							filter: 'isFile'
						},
						{
							expand: true,
							flatten: true,
							src: ['./assets/favicon.ico'],
							dest: './deploy/assets/',
							filter: 'isFile'
						},
						{
							expand: true,
							flatten: true,
							src: ['./bower_components/components-font-awesome/css/font-awesome.min.css'],
							dest: './deploy/assets/styles/css/',
							filter: 'isFile'
						},
						{
							expand: true,
							flatten: true,
							src: ['./bower_components/angular-cookie-law/dist/angular-cookie-law.min.css'],
							dest: './deploy/assets/styles/css/',
							filter: 'isFile'
						},
						{
							expand: true,
							flatten: true,
							src: ['./bower_components/components-font-awesome/fonts/*'],
							dest: './deploy/assets/styles/fonts/',
							filter: 'isFile'
						}
					]
				}
			}

		});

		grunt.loadNpmTasks('grunt-contrib-sass');
		grunt.loadNpmTasks('grunt-contrib-watch');
		grunt.loadNpmTasks('grunt-bower-install');
		grunt.loadNpmTasks('grunt-contrib-concat');
		grunt.loadNpmTasks('grunt-contrib-uglify');
		grunt.loadNpmTasks('grunt-processhtml');
		grunt.loadNpmTasks('grunt-contrib-copy');

		grunt.registerTask('default', ['watch']);
		grunt.registerTask('generateDeploy', ['processhtml', 'copy']);
	};
})();