(function() {
	'use strict';

	var aboutController = function($scope, CookieLawService) {
		var vm = this;

		vm.cookies = CookieLawService.isEnabled();

		$scope.$on('cookieLaw.dismiss', function() {
			vm.cookies = true;
		});
	};

	aboutController.$inject = [
		'$scope',
		'CookieLawService'
	];

	/*global angular*/
	angular
		.module('izpovedi')
		.controller('aboutController', aboutController);
})();