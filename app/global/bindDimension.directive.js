(function() {
	'use strict';

	var bindDimension = function($window, $timeout) {
		return {
			link: linkFunction,
			restrict: 'A'
		};

		function linkFunction(scope, element, attrs) {
			scope.windowWidth = $window.innerWidth;

			$timeout(function() {

				if(attrs.adtype === 'rlg') {
					scope.rightLargeWidth = element[0].offsetWidth;
				} else if(attrs.adtype === 'rsm') {
					scope.rightSmallWidth = element[0].offsetWidth;
				} else if(attrs.adtype === 'tlg') {
					scope.topLargeHeight = element[0].offsetHeight;
				} else if(attrs.adtype === 'tsm') {
					scope.topSmallHeight = element[0].offsetHeight;
				}

			}, 0);

			angular.element($window).bind('resize', function() {
				if(attrs.adtype === 'rlg') {
					scope.rightLargeWidth = element[0].offsetWidth;
				} else if(attrs.adtype === 'rsm') {
					scope.rightSmallWidth = element[0].offsetWidth;
				} else if(attrs.adtype === 'tlg') {
					scope.topLargeHeight = element[0].offsetHeight;
				} else if(attrs.adtype === 'tsm') {
					scope.topSmallHeight = element[0].offsetHeight;
				}

				scope.$digest();
			});
		}
	};

	bindDimension.$inject = [
		'$window',
		'$timeout'
	];

	/*global angular*/
	angular
		.module('izpovedi')
		.directive('bindDimension', bindDimension);
})();