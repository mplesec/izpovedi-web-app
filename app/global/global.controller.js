(function() {
	'use strict';

	var globalController = function() {
		var vm = this;
		vm.topSmallHeight = null;
		vm.topLargeHeight = null;
		vm.rightLargeWidth = null;
		vm.rightSmallWidth = null;
	};

	/*global angular*/
	angular
		.module('izpovedi')
		.controller('globalController', globalController);
})();