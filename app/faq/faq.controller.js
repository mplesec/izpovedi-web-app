(function() {
	'use strict';

	var faqController = function($scope, CookieLawService) {
		var vm = this;

		vm.cookies = CookieLawService.isEnabled();

		$scope.$on('cookieLaw.dismiss', function() {
			vm.cookies = true;
		});
	};

	faqController.$inject = [
		'$scope',
		'CookieLawService'
	];

	/*global angular*/
	angular
		.module('izpovedi')
		.controller('faqController', faqController);
})();