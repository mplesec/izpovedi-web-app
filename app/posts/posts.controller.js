(function() {
	'use strict';

	var postsController = function($scope, postsCallsService, $stateParams, $location, $timeout, postVoteService, CookieLawService, reportService, $state) {
		var sorting = (typeof $stateParams.prikaz === 'undefined') ? 'top' : $stateParams.prikaz;
		var timeSort = (typeof $stateParams.cas === 'undefined') ? 'mesec' : $stateParams.cas;
		var location = '';
		var vm = this;

		// Variables
		vm.cookies = CookieLawService.isEnabled();
		vm.placeholder = 'Objavi svojo izpoved.';
		vm.submitText = '';
		vm.textTooShort = false;
		vm.textTooLong = false;
		vm.countDown = false;
		vm.showAlert = true;
		vm.countDownTime = 0;
		vm.successMessage = '';
		vm.page = $stateParams.stran;
		vm.activeVotes = [];

		// Functions
		vm.sortChange = sortChange;
		vm.timeChange = timeChange;
		vm.pageChange = pageChange;
		vm.report = report;
		vm.vote = vote;
		vm.countDownFinished = countDownFinished;
		vm.submit = submit;
		vm.share = share;

		// Logic
		if(typeof vm.page === 'undefined') {
			vm.page = 0;
		}

		if(['rastoce', 'novo'].indexOf(sorting) !== -1) {
			vm.sort = sorting;
		} else {
			vm.sort = 'top';
		}

		if(['dan', 'teden', 'vse'].indexOf(timeSort) !== -1) {
			vm.time = timeSort;
		} else {
			vm.time = 'mesec';
		}

		postsCallsService.remainingTime(
			function(response) {
				if(typeof response.data.content.timeRemaining !== 'undefined') {
					vm.countDownTime = parseInt(response.data.content.timeRemaining);
					vm.countDown = true;
				} else {
					vm.showAlert = false;
				}
			},
			function(response) {
				console.log(response);
			}
		);

		postsCallsService.getPosts(vm.page, sorting, timeSort,
			function(response) {
				if(typeof response === 'undefined') {
					vm.sortChange(vm.sort);
				} else {
					for(var i = 0; i < response.length; i++) {
						vm.activeVotes.push({
							id: response[i].id,
							like: false,
							dislike: false
						});
					}
					vm.postsArray = response;
				}

				postsCallsService.getPosts(parseInt(vm.page) + 1, sorting, timeSort,
					function(response) {
						if(typeof response === 'undefined') {
							vm.nextPage = 0;
						} else if(response.length > 0) {
							vm.nextPage = vm.page + 1;
						} else {
							vm.nextPage = 0;
						}
					}, function(response) {
						console.log(response);
					}
				);
			}, function(response) {
				console.log(response);
			}
		);

		$scope.$on('cookieLaw.dismiss', function() {
			vm.cookies = true;
		});

		// Function definitions
		function sortChange(newSort) {
			location = '?prikaz=' + newSort + '&cas=' + vm.time;
			$location.url(location);
		}

		function timeChange(newTime) {
			location = '?prikaz=' + vm.sort + '&cas=' + newTime;
			$location.url(location);
		}

		function pageChange(direction) {
			location = '?prikaz=' + vm.sort + '&cas=' + vm.time + '&stran=';

			if(direction === 'next') {
				location = location + (parseInt(vm.page) + 1);
			} else if(direction === 'prev') {
				location = location + (parseInt(vm.page) - 1);
			}

			$location.url(location);
		}

		function report(id) {
			reportService.reportPost(id,
				function(response) {
					if(response.data.message.indexOf('Report on this post for this user already exists') === -1) {
						var post = vm.postsArray.filter(function(obj) {
							return obj.id === id;
						})[0];

						post.report = true;
					} else {
						reportService.deletePostReport(id,
							function(response) {
								if(response.data.message.indexOf('Report deleted') !== -1) {
									var post = vm.postsArray.filter(function(obj) {
										return obj.id === id;
									})[0];

									post.report = false;
								}
							},
							function(response) {
								console.log(response);
							}
						);
					}
				},
				function(response) {
					console.log(response);
				}
			);
		}

		function vote(choice, postID) {
			var votes = vm.activeVotes.filter(function(obj) {
				return obj.id === postID;
			})[0];

			if((choice === 'like' && votes.like === false) || (choice === 'dislike' && votes.dislike === false)) {
				var post = vm.postsArray.filter(function(obj) {
					return obj.id === postID;
				})[0];

				if(choice === 'like') {
					votes.like = true;
				} else if(choice === 'dislike') {
					votes.dislike = true;
				}

				post = vm.postsArray[vm.postsArray.indexOf(post)];

				var method = (post.vote.choice == null) ? false : true;

				postVoteService.vote(choice, postID, method,
					function(response) {
						if(response.data.message.indexOf('Vote choice on this post for this user is already:') === -1 && response.data.message.indexOf('Vote on this post for this user does not exist') === -1 && response.data.message.indexOf('Vote on this post for this user already exists') === -1) {
							if(choice === 'like') {
								votes.like = false;
							} else if(choice === 'dislike') {
								votes.dislike = false;
							}

							post.vote.choice = (choice === 'like') ? true : false;

							if(choice === 'like' && method === true) {
								post.vote.count[0].upvotes = post.vote.count[0].upvotes + 1;
								post.vote.count[0].downvotes = post.vote.count[0].downvotes - 1;
							} else if(choice === 'like' && method === false) {
								post.vote.count[0].upvotes = post.vote.count[0].upvotes + 1;
							} else if(choice === 'dislike' && method === true) {
								post.vote.count[0].upvotes = post.vote.count[0].upvotes - 1;
								post.vote.count[0].downvotes = post.vote.count[0].downvotes + 1;
							} else if(choice === 'dislike' && method === false) {
								post.vote.count[0].downvotes = post.vote.count[0].downvotes + 1;
							}
						} else {
							postVoteService.deleteVote(postID,
								function(response) {
									if(response.data.message.indexOf('Vote on this post for this user does not exist') === -1) {
										if(choice === 'like') {
											votes.like = false;
										} else if(choice === 'dislike') {
											votes.dislike = false;
										}

										post.vote.choice = null;

										if(choice === 'like') {
											post.vote.count[0].upvotes =
												post.vote.count[0].upvotes - 1;
										} else if(choice === 'dislike') {
											post.vote.count[0].downvotes =
												post.vote.count[0].downvotes - 1;
										}
									}
								},
								function(response) {
									if(choice === 'like') {
										votes.like = false;
									} else if(choice === 'dislike') {
										votes.dislike = false;
									}

									console.log(response);
								}
							);
						}
					},
					function(response) {
						if(choice === 'like') {
							votes.like = false;
						} else if(choice === 'dislike') {
							votes.dislike = false;
						}

						console.log(response);
					}
				);
			}
		}

		function countDownFinished() {
			vm.countDown = false;
			vm.showAlert = false;
			vm.countDownTime = 0;
			$scope.$apply();
		}

		function submit() {
			var submitText = vm.submitText;
			var length = submitText.replace(/\s/g, '').length;

			if(length < 10) {
				vm.showAlert = true;
				vm.textTooShort = true;

				$timeout(function() {
					vm.textTooShort = false;
					vm.showAlert = false;
				}, 2000);
			} else if(length > 1000) {
				vm.showAlert = true;
				vm.textTooLong = true;

				$timeout(function() {
					vm.textTooLong = false;
					vm.showAlert = false;
				}, 2000);
			} else {
				submitText = submitText.replace(/\s/g, ' ');

				postsCallsService.submit(submitText,
					function() {
						vm.successMessage = 'Čestitam! Uspešno si objavil/a svojo izpoved. Najdeš jo lahko pod novimi izpovedmi. ';

						$state.transitionTo($state.current,
							{
								prikaz: 'novo',
								cas: 'vse'
							},
							{
								reload: true,
								inherit: false,
								notify: true
							}
						);
					},
					function(response) {
						console.log(response);
					}
				);
			}
		}

		function share(postID, text) {
			var words = text.split(' ');
			var name = words.slice(0, 4).join(' ');
			text = words.slice(4, 30).join(' ');

			if(words.length > 30 || text.length === 0) {
				text = text + ' ...';
			}

			/*global FB*/
			FB.ui({
				method: 'feed',
				link: 'http://www.izpovedi.si/i/'+postID,
				name: name,
				description: text,
				picture: 'http://izpovedi.si/assets/images/fbshare.png'
			}, function() {});
		}
	};

	postsController.$inject = [
		'$scope',
		'postsCallsService',
		'$stateParams',
		'$location',
		'$timeout',
		'postVoteService',
		'CookieLawService',
		'reportService',
		'$state'
	];

	/*global angular*/
	angular
		.module('izpovedi')
		.controller('postsController', postsController);
})();