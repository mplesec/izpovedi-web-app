(function() {
	'use strict';

	var postsCallsService = function($window, $http, uidService, apiurl) {
		var postsCalls = {
			getPosts: getPosts,
			submit: submit,
			remainingTime: remainingTime
		};

		return postsCalls;

		function getPosts(page, sorting, timeSort, successHandler, errorHandler) {
			var url = '';

			if(sorting === 'top') {
				url = apiurl + 'posts/top';
			} else if (sorting === 'novo') {
				url = apiurl + 'posts/new';
			} else {
				url = apiurl + 'posts/rising';
			}

			var paramsObj = {
				page: page
			};

			if(timeSort === 'dan' && sorting === 'top') {
				paramsObj.time = 'day';
			} else if(timeSort === 'teden' && sorting === 'top') {
				paramsObj.time = 'week';
			} else if(timeSort === 'mesec' && sorting === 'top') {
				paramsObj.time = 'month';
			}

			var config;

			if(typeof uidService.getUID() !== 'undefined') {
				config = {
					url: url,
					method: 'GET',
					params: paramsObj,
					headers: {
						Authorization: uidService.getUID()
					}
				};
			} else {
				config = {
					url: url,
					method: 'GET',
					params: paramsObj
				};
			}

			$http(
				config
			).then(
				function successCallback(response) {
					if(typeof response.data.verification.uid !== 'undefined') {
						uidService.saveUID(response.data.verification.uid);
					}
					var responseArray = response.data.content.posts,
						tempTime,
						timeString,
						responseMessage = response.data.message;

					if(typeof responseArray === 'undefined') {
						successHandler(responseArray);
					} else {
						for(var i = 0; i < responseArray.length; i++) {
							tempTime = new Date(responseArray[i].created_at);

							timeString = '';

							timeString = tempTime.getDate() + '.' + (tempTime.getMonth() + 1) + '.' + tempTime.getFullYear() + ' ' + tempTime.getHours() + 'h ' + tempTime.getMinutes() + 'min';

							responseArray[i].created_at = timeString;
						}

						successHandler(responseArray, responseMessage);
					}
				},
				function errorCallback(response) {
					errorHandler(response);
				}
			);
		}

		function submit(text, successHandler, errorHandler) {
			var config;

			if(typeof uidService.getUID() !== 'undefined') {
				config = {
					url: apiurl + 'post',
					method: 'POST',
					params: {text: text},
					headers: {
						Authorization: uidService.getUID()
					}
				};
			} else {
				config = {
					url: apiurl + 'post',
					method: 'POST',
					params: {text: text}
				};
			}

			$http(
				config
			).then(
				function successCallback(response) {
					if(response.data.verification.hasOwnProperty('uid')) {
						uidService.saveUID(response.data.verification.uid);
					}
					/*if(typeof response.data.verification.uid != 'undefined') {
						uidService.saveUID(response.data.verification.uid);
					}*/
					successHandler(response);
				},
				function errorCallback(response) {
					errorHandler(response);
				}
			);
		}

		function remainingTime(successHandler, errorHandler) {
			var config;

			if(typeof uidService.getUID() !== 'undefined') {
				config = {
					url: apiurl + 'user/time-remaining/post',
					method: 'GET',
					headers: {
						Authorization: uidService.getUID()
					}
				};
			} else {
				config = {
					url: apiurl + 'user/time-remaining/post',
					method: 'GET'
				};
			}

			$http(
				config
			).then(
				function successCallback(response) {
					if(typeof response.data.verification.uid !== 'undefined') {
						uidService.saveUID(response.data.verification.uid);
					}
					successHandler(response);
				},
				function errorCallback(response) {
					errorHandler(response.status);
				}
			);
		}
	};

	postsCallsService.$inject = [
		'$window',
		'$http',
		'uidService',
		'apiurl'
	];

	/*global angular*/
	angular
		.module('izpovedi')
		.factory('postsCallsService', postsCallsService);
})();