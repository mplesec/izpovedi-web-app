(function() {
	'use strict';

	var config = function($stateProvider, $urlRouterProvider, $locationProvider) {
		// State definitions
		$stateProvider.state('posts', {
			url: '/?prikaz?cas?stran',
			templateUrl: 'app/templates/posts.html',
			controller: 'postsController as vm'
		})
		.state('i', {
			url: '/i/{id}?stran',
			templateUrl: 'app/templates/post.html',
			controller: 'postController as vm'
		})
		.state('opis', {
			url: '/opis',
			templateUrl: 'app/templates/opis.html',
			controller: 'aboutController as vm'
		})
		.state('faq', {
			url: '/faq',
			templateUrl: 'app/templates/faq.html',
			controller: 'faqController as vm'
		});

		$urlRouterProvider.otherwise('/');

		// Disable usage of # in navigation
		$locationProvider.html5Mode(true);
	};

	config.$inject = [
		'$stateProvider',
		'$urlRouterProvider',
		'$locationProvider'
	];

	/*global angular*/
	angular
		.module('izpovedi')
		.config(config);
})();