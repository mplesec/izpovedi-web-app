(function() {
	'use strict';

	var noSpaceCount = function() {
		return function(input) {
			return input.replace(/\s/g, '').length;
		};
	};

	/*global angular*/
	angular
		.module('izpovedi')
		.filter('noSpaceCount', noSpaceCount);
})();