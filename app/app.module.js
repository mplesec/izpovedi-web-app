(function() {
	'use strict';

	var run = function($window) {
		/*global FB*/
		$window.fbAsyncInit = function() {
			FB.init({
				appId: '1089922971071680',
				xfbml: true,
				version: 'v2.5'
			});
		};

		(function(d) {
			// Load the Facebook javascript SDK
			var js;
			var id = 'facebook-jssdk';
			var ref = d.getElementsByTagName('script')[0];

			if (d.getElementById(id)) {
				return;
			}

			js = d.createElement('script');
			js.id = id;
			js.async = true;
			js.src = '//connect.facebook.net/sl_SI/sdk.js';

			ref.parentNode.insertBefore(js, ref);
		}(document));
	};

	run.$inject = [
		'$window'
	];

	/*global angular*/
	angular
		.module('izpovedi', ['ui.router', 'ui.bootstrap', 'timer', 'angulartics', 'angulartics.google.analytics', 'angular-cookie-law'])
		.run(run);

})();
