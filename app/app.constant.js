(function() {
	'use strict';

	/*global angular*/
	angular
		.module('izpovedi')
		.constant('apiurl', 'http://izpovedi.si/api/');
})();