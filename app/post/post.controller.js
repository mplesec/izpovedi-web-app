(function() {
	'use strict';

	var postController = function($scope, $stateParams, postCallsService, $location, $timeout, commentVoteService, postVoteService, CookieLawService, reportService) {
		var vm = this;
		var id = $stateParams.id;

		// Variables
		vm.cookies = CookieLawService.isEnabled();
		vm.placeholder = 'Objavi svoj komentar.';
		vm.submitText = '';
		vm.textTooShort = false;
		vm.textTooLong = false;
		vm.countDown = false;
		vm.showAlert = true;
		vm.countDownTime = 0;
		vm.successMessage = '';
		vm.page = $stateParams.stran;
		vm.activeVotesComments = [];
		vm.activeVotesFeaturedComments = [];
		vm.like = false;
		vm.dislike = false;

		// Functions
		vm.pageChange = pageChange;
		vm.getComments = getComments;
		vm.getTopComments = getTopComments;
		vm.vote = vote;
		vm.commentVote = commentVote;
		vm.countDownFinished = countDownFinished;
		vm.submit = submit;
		vm.share = share;
		vm.reportPost = reportPost;
		vm.reportComment = reportComment;

		// Logic
		if(typeof vm.page === 'undefined') {
			vm.page = 0;
		}

		postCallsService.getPost(id,
			function(response) {
				if(response.data.message === 'Post does not exist') {
					$location.url('');
				} else {
					vm.postData = response.data.content.post;
				}
			},
			function(response) {
				console.log(response);
			}
		);

		vm.getComments(id, vm.page);

		vm.getTopComments(id);

		postCallsService.remainingTime(id,
			function(response) {
				if(typeof response.data.content.timeRemaining !== 'undefined') {
					vm.countDownTime = parseInt(response.data.content.timeRemaining);
					vm.countDown = true;
				} else {
					vm.showAlert = false;
				}
			},
			function(response) {
				console.log(response);
			}
		);

		$scope.$on('cookieLaw.dismiss', function() {
			vm.cookies = true;
		});

		// Function definitions
		function pageChange(direction) {
			var location = '/i/' + id + '?stran=';

			if(direction === 'next') {
				location = location + (parseInt(vm.page) + 1);
			} else if(direction === 'prev') {
				location = location + (parseInt(vm.page) - 1);
			}

			$location.url(location);
		}

		function getComments(id, page) {
			postCallsService.getComments(id, page,
				function(response) {
					if(typeof response !== 'undefined') {
						vm.activeVotesComments = [];
						for(var i = 0; i < response.length; i++) {
							vm.activeVotesComments.push({
								id: response[i].id,
								like: false,
								dislike: false
							});
						}

						vm.commentArray = response;

						postCallsService.getComments(id, parseInt(vm.page) + 1,
							function(response) {
								if(typeof response === 'undefined') {
									vm.nextPage = 0;
								} else if(response.length > 0) {
									vm.nextPage = vm.page + 1;
								} else {
									vm.nextPage = 0;
								}
							},
							function(response) {
								console.log(response);
							}
						);
					}
				},
				function(response) {
					console.log(response);
				}
			);
		}

		function getTopComments(id) {
			postCallsService.getTopComments(id,
				function(response) {
					if(typeof response !== 'undefined') {
						vm.activeVotesFeaturedComments = [];
						for(var i = 0; i < response.length; i++) {
							vm.activeVotesFeaturedComments.push({
								id: response[i].id,
								like: false,
								dislike: false
							});
						}

						vm.topCommentArray = response;
					}
				},
				function(response) {
					console.log(response);
				}
			);
		}

		function vote(choice, postID) {
			if((choice === 'like' && vm.like === false) || (choice === 'dislike' && vm.dislike === false)) {
				var method = (vm.postData.vote.choice == null) ? false : true;
				var counts = vm.postData.vote.count[0];

				if(choice === 'like') {
					vm.like = true;
				} else if(choice === 'dislike') {
					vm.dislike = true;
				}

				postVoteService.vote(choice, postID, method,
					function(response) {
						if(response.data.message.indexOf('Vote choice on this post for this user is already:') === -1 && response.data.message.indexOf('Vote on this post for this user does not exist') === -1 && response.data.message.indexOf('Vote on this post for this user already exists') === -1) {
							if(choice === 'like') {
								vm.like = false;
							} else if(choice === 'dislike') {
								vm.dislike = false;
							}

							vm.postData.vote.choice = (choice === 'like') ? true : false;

							if(choice === 'like' && method === true) {
								counts.upvotes = counts.upvotes + 1;
								counts.downvotes = counts.downvotes - 1;
							} else if(choice === 'like' && method === false) {
								counts.upvotes = counts.upvotes + 1;
							} else if(choice === 'dislike' && method === true) {
								counts.upvotes = counts.upvotes - 1;
								counts.downvotes = counts.downvotes + 1;
							} else if(choice === 'dislike' && method === false) {
								counts.downvotes = counts.downvotes + 1;
							}
						} else {
							postVoteService.deleteVote(postID,
								function(response) {
									if(choice === 'like') {
										vm.like = false;
									} else if(choice === 'dislike') {
										vm.dislike = false;
									}

									if(response.data.message.indexOf('Vote on this post for this user does not exist') === -1) {
										vm.postData.vote.choice = null;

										if(choice === 'like') {
											counts.upvotes = counts.upvotes - 1;
										} else if(choice === 'dislike') {
											counts.downvotes = counts.downvotes - 1;
										}
									}
								},
								function(response) {
									if(choice === 'like') {
										vm.like = false;
									} else if(choice === 'dislike') {
										vm.dislike = false;
									}

									console.log(response);
								}
							);
						}
					},
					function(response) {
						if(choice === 'like') {
							vm.like = false;
						} else if(choice === 'dislike') {
							vm.dislike = false;
						}

						console.log(response);
					}
				);
			}
		}

		function commentVote(choice, commentID) {

			var votedComment = vm.activeVotesComments.filter(function(obj) {
				return obj.id === commentID;
			})[0];

			var votedFeaturedComment = vm.activeVotesFeaturedComments
				.filter(function(obj) {
					return obj.id === commentID;
				})[0];

			var canVote = false;

			if(typeof votedComment !== 'undefined') {
				if((choice === 'like' && votedComment.like === false) || (choice === 'dislike' && votedComment.dislike === false)) {
					canVote = true;
				}
			} else if(typeof votedFeaturedComment !== 'undefined') {
				if((choice === 'like' && votedFeaturedComment.like === false) || (choice === 'dislike' && votedFeaturedComment.dislike === false)) {
					canVote = true;
				}
			}

			if(canVote) {
				if(typeof votedComment !== 'undefined') {
					if(choice === 'like') {
						votedComment.like = true;
					} else if(choice === 'dislike') {
						votedComment.dislike = true;
					}
				}

				if(typeof votedFeaturedComment !== 'undefined') {
					if(choice === 'like') {
						votedFeaturedComment.like = true;
					} else if(choice === 'dislike') {
						votedFeaturedComment.dislike = true;
					}
				}

				commentVoteService.vote(choice, commentID,
					vm, votedComment, votedFeaturedComment,
					function(commentArray, topCommentArray,
						newVotedComment, newVotedFeaturedComment) {
						if(commentArray == null || topCommentArray == null) {
							votedComment = newVotedComment;
							votedFeaturedComment = newVotedFeaturedComment;
						} else {
							votedComment = newVotedComment;
							votedFeaturedComment = newVotedFeaturedComment;

							vm.commentArray = commentArray;
							vm.topCommentArray = topCommentArray;
						}
					}
				);
			}
		}

		function countDownFinished() {
			vm.countDown = false;
			vm.showAlert = false;
			vm.countDownTime = 0;
			$scope.$apply();
		}

		function submit() {
			var submitText = vm.submitText;
			var length = submitText.replace(/\s/g, '').length;

			if(length < 10) {
				vm.showAlert = true;
				vm.textTooShort = true;
				$timeout(function() {vm.textTooShort = false; vm.showAlert = false;}, 2000);
			} else if(length > 1000) {
				vm.showAlert = true;
				vm.textTooLong = true;
				$timeout(function() {vm.textTooLong = false; vm.showAlert = false;}, 2000);
			} else {
				submitText = submitText.replace(/\s/g, ' ');

				postCallsService.submit(id, submitText,
					function() {
						vm.successMessage = 'Čestitam! Uspešno si komentiral/a na izpoved. ';

						vm.getComments(id, 0);
						vm.getTopComments(id);

						postCallsService.remainingTime(id,
							function(response) {
								if(typeof response.data.content.timeRemaining !== 'undefined') {
									vm.countDownTime =
										parseInt(response.data.content.timeRemaining);
									vm.showAlert = true;
									vm.countDown = true;
								}
							},
							function(response) {
								console.log(response);
							}
						);
					},
					function(response) {
						console.log(response);
					}
				);
			}
		}

		function share(postID, text) {
			var words = text.split(' ');
			var name = words.slice(0, 4).join(' ');
			text = words.slice(4, 30).join(' ');

			if(words.length > 30 || text.length === 0) {
				text = text + ' ...';
			}

			/*global FB*/
			FB.ui({
				method: 'feed',
				link: 'http://www.izpovedi.si/i/'+postID,
				name: name,
				description: text,
				picture: 'http://izpovedi.si/assets/images/fbshare.png'
			}, function() {});
		}

		function reportPost(id) {
			reportService.reportPost(id,
				function(response) {
					if(response.data.message.indexOf('Report on this post for this user already exists') === -1) {
						vm.postData.report = true;
					} else {
						reportService.deletePostReport(id,
							function(response) {
								if(response.data.message.indexOf('Report deleted') !== -1) {
									vm.postData.report = false;
								}
							},
							function(response) {
								console.log(response);
							}
						);
					}
				},
				function(response) {
					console.log(response);
				}
			);
		}

		function reportComment(id) {
			reportService.reportComment(id,
				function(response) {
					if(response.data.message.indexOf('Report on this comment for this user already exists') === -1) {
						var comment = vm.commentArray.filter(function(obj) {
							return obj.id === id;
						})[0];

						var topComment = vm.topCommentArray
						.filter(function(obj) {
							return obj.id === id;
						})[0];

						if(typeof comment !== 'undefined') {
							comment.report = true;
						}

						if(typeof topComment !== 'undefined') {
							topComment.report = true;
						}
					} else {
						reportService.deleteCommentReport(id,
							function(response) {
								if(response.data.message.indexOf('Report deleted') !== -1) {
									var comment = vm.commentArray.filter(function(obj) {
										return obj.id === id;
									})[0];

									var topComment = vm.topCommentArray
									.filter(function(obj) {
										return obj.id === id;
									})[0];

									if(typeof comment !== 'undefined') {
										comment.report = false;
									}

									if(typeof topComment !== 'undefined') {
										topComment.report = false;
									}
								}
							},
							function(response) {
								console.log(response);
							}
						);
					}
				},
				function(response) {
					console.log(response);
				}
			);
		}
	};

	postController.$inject = [
		'$scope',
		'$stateParams',
		'postCallsService',
		'$location',
		'$timeout',
		'commentVoteService',
		'postVoteService',
		'CookieLawService',
		'reportService'
	];

	/*global angular*/
	angular
		.module('izpovedi')
		.controller('postController', postController);
})();