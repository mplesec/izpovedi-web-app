(function() {
	'use strict';

	var commentVoteService = function(postCallsService) {
		var commentVote = {
			vote: vote
		};

		return commentVote;

		function vote(choice, commentID, post, votedComment, votedFeaturedComment, assignVal) {
			var comment = post.commentArray.filter(function(obj) {
				return obj.id === commentID;
			})[0];

			var topComment = post.topCommentArray.filter(function(obj) {
				return obj.id === commentID;
			})[0];

			var method;

			if(typeof comment === 'undefined') {
				method = (topComment.vote.choice == null) ? false : true;
			} else {
				method = (comment.vote.choice == null) ? false : true;
			}

			comment = post.commentArray[post.commentArray.indexOf(comment)];
			topComment = post.topCommentArray[post.topCommentArray.indexOf(topComment)];

			postCallsService.commentVote(choice, commentID, method,
				function(response) {
					if(response.data.message.indexOf('Vote choice on this comment for this user is already:') === -1) {
						if(typeof comment !== 'undefined') {
							comment.vote.choice = (choice === 'like') ? true : false;
						}

						if(typeof topComment !== 'undefined') {
							topComment.vote.choice = (choice === 'like') ? true : false;
						}

						if(choice === 'like' && method === true) {
							if(typeof comment !== 'undefined') {
								comment.vote.count[0].upvotes = comment.vote.count[0].upvotes + 1;

								comment.vote.count[0].downvotes = comment.vote.count[0].downvotes - 1;
							}

							if(typeof topComment !== 'undefined') {
								topComment.vote.count[0].upvotes = topComment.vote.count[0].upvotes + 1;

								topComment.vote.count[0].downvotes = topComment.vote.count[0].downvotes - 1;
							}
						} else if(choice === 'like' && method === false) {
							if(typeof comment !== 'undefined') {
								comment.vote.count[0].upvotes = comment.vote.count[0].upvotes + 1;
							}
							if(typeof topComment !== 'undefined') {
								topComment.vote.count[0].upvotes = topComment.vote.count[0].upvotes + 1;
							}
						} else if(choice === 'dislike' && method === true) {
							if(typeof comment !== 'undefined') {
								comment.vote.count[0].upvotes = comment.vote.count[0].upvotes - 1;

								comment.vote.count[0].downvotes = comment.vote.count[0].downvotes + 1;
							}
							if(typeof topComment !== 'undefined') {
								topComment.vote.count[0].upvotes = topComment.vote.count[0].upvotes - 1;
								topComment.vote.count[0].downvotes = topComment.vote.count[0].downvotes + 1;
							}
						} else if(choice === 'dislike' && method === false) {
							if(typeof comment !== 'undefined') {
								comment.vote.count[0].downvotes = comment.vote.count[0].downvotes + 1;
							}
							if(typeof topComment !== 'undefined') {
								topComment.vote.count[0].downvotes = topComment.vote.count[0].downvotes + 1;
							}
						}

						if(typeof votedComment !== 'undefined') {
							if(choice === 'like') {
								votedComment.like = false;
							} else if(choice === 'dislike') {
								votedComment.dislike = false;
							}
						}

						if(typeof votedFeaturedComment !== 'undefined') {
							if(choice === 'like') {
								votedFeaturedComment.like = false;
							} else if(choice === 'dislike') {
								votedFeaturedComment.dislike = false;
							}
						}

						assignVal(post.commentArray, post.topCommentArray, votedComment, votedFeaturedComment);
					} else {
						postCallsService.deleteCommentVote(commentID,
							function() {

								if(typeof comment !== 'undefined') {
									comment.vote.choice = null;
								}

								if(typeof topComment !== 'undefined') {
									topComment.vote.choice = null;
								}
								if(choice === 'like') {
									if(typeof comment !== 'undefined') {
										comment.vote.count[0].upvotes = comment.vote.count[0].upvotes - 1;
									}

									if(typeof topComment !== 'undefined') {
										topComment.vote.count[0].upvotes = topComment.vote.count[0].upvotes - 1;
									}
								} else if(choice === 'dislike') {
									if(typeof comment !== 'undefined') {
										comment.vote.count[0].downvotes = comment.vote.count[0].downvotes - 1;
									}

									if(typeof topComment !== 'undefined') {
										topComment.vote.count[0].downvotes = topComment.vote.count[0].downvotes - 1;
									}
								}

								if(typeof votedComment !== 'undefined') {
									if(choice === 'like') {
										votedComment.like = false;
									} else if(choice === 'dislike') {
										votedComment.dislike = false;
									}
								}

								if(typeof votedFeaturedComment !== 'undefined') {
									if(choice === 'like') {
										votedFeaturedComment.like = false;
									} else if(choice === 'dislike') {
										votedFeaturedComment.dislike = false;
									}
								}

								assignVal(post.commentArray, post.topCommentArray, votedComment, votedFeaturedComment);
							},
							function() {
								if(typeof votedComment !== 'undefined') {
									if(choice === 'like') {
										votedComment.like = false;
									} else if(choice === 'dislike') {
										votedComment.dislike = false;
									}
								}

								if(typeof votedFeaturedComment !== 'undefined') {
									if(choice === 'like') {
										votedFeaturedComment.like = false;
									} else if(choice === 'dislike') {
										votedFeaturedComment.dislike = false;
									}
								}

								assignVal(null, null, votedComment, votedFeaturedComment);
							}
						);
					}
				},
				function() {
					if(typeof votedComment !== 'undefined') {
						if(choice === 'like') {
							votedComment.like = false;
						} else if(choice === 'dislike') {
							votedComment.dislike = false;
						}
					}

					if(typeof votedFeaturedComment !== 'undefined') {
						if(choice === 'like') {
							votedFeaturedComment.like = false;
						} else if(choice === 'dislike') {
							votedFeaturedComment.dislike = false;
						}
					}

					assignVal(null, null, votedComment, votedFeaturedComment);
				}
			);
		}
	};

	commentVoteService.$inject = ['postCallsService'];

	/*global angular*/
	angular
		.module('izpovedi')
		.factory('commentVoteService', commentVoteService);
})();