(function() {
	'use strict';

	var postCallsService = function($window, $http, uidService, apiurl) {
		var postCalls = {
			getPost: getPost,
			getComments: getComments,
			getTopComments: getTopComments,
			commentVote: commentVote,
			deleteCommentVote: deleteCommentVote,
			remainingTime: remainingTime,
			submit: submit
		};

		return postCalls;

		function getPost(id, successHandler, errorHandler) {
			var config;

			if(typeof uidService.getUID() !== 'undefined') {
				config = {
					url: apiurl + 'post/' + id,
					method: 'GET',
					headers: {
						Authorization: uidService.getUID()
					}
				};
			} else {
				config = {
					url: apiurl + 'post/' + id,
					method: 'GET'
				};
			}

			$http(
				config
			).then(
				function successCallback(response) {
					if(typeof response.data.verification.uid !== 'undefined') {
						uidService.saveUID(response.data.verification.uid);
					}
					successHandler(response);
				},
				function errorCallback(response) {
					errorHandler(response);
				}
			);
		}

		function getComments(postID, page, successHandler, errorHandler) {

			var paramsObj = {page: page};

			var config;

			if(typeof uidService.getUID() !== 'undefined') {
				config = {
					url: apiurl + 'post/' + postID + '/comments/new',
					method: 'GET',
					params: paramsObj,
					headers: {
						Authorization: uidService.getUID()
					}
				};
			} else {
				config = {
					url: apiurl + 'post/' + postID + '/comments/new',
					method: 'GET',
					params: paramsObj
				};
			}

			$http(
				config
			).then(
				function successCallback(response) {
					if(typeof response.data.verification.uid !== 'undefined') {
						uidService.saveUID(response.data.verification.uid);
					}
					var responseArray = response.data.content.comments;
					var tempTime;
					var timeString;
					var responseMessage = response.data.message;

					if(typeof responseArray === 'undefined') {
						successHandler(responseArray);
					} else {
						for(var i = 0; i < responseArray.length; i++) {
							tempTime = new Date(responseArray[i].created_at);

							timeString = '';

							timeString = tempTime.getDate() + '.' + (tempTime.getMonth() + 1) + '.' + tempTime.getFullYear() + ' ' + tempTime.getHours() + 'h ' + tempTime.getMinutes() + 'min';

							responseArray[i].created_at = timeString;
						}

						successHandler(responseArray, responseMessage);
					}
				},
				function errorCallback(response) {
					errorHandler(response);
				}
			);
		}

		function getTopComments(postID, successHandler, errorHandler) {
			var config;

			if(typeof uidService.getUID() !== 'undefined') {
				config = {
					url: apiurl + 'post/' + postID + '/comments/top',
					method: 'GET',
					headers: {
						Authorization: uidService.getUID()
					}
				};
			} else {
				config = {
					url: apiurl + 'post/' + postID + '/comments/top',
					method: 'GET'
				};
			}

			$http(
				config
			).then(
				function successCallback(response) {

					if(typeof response.data.verification.uid !== 'undefined') {
						uidService.saveUID(response.data.verification.uid);
					}
					var responseArray = response.data.content.comments;
					var tempTime;
					var timeString;
					var responseMessage = response.data.message;

					if(typeof responseArray === 'undefined') {
						successHandler(responseArray);
					} else {
						for(var i = 0; i < responseArray.length; i++) {
							tempTime = new Date(responseArray[i].created_at);

							timeString = '';

							timeString = tempTime.getDate() + '.' + (tempTime.getMonth() + 1) + '.' + tempTime.getFullYear() + ' ' + tempTime.getHours() + 'h ' + tempTime.getMinutes() + 'min';

							responseArray[i].created_at = timeString;
						}

						successHandler(responseArray, responseMessage);
					}
				},
				function errorCallback(response) {
					errorHandler(response);
				}
			);
		}

		function commentVote(choice, commentID, update, successHandler, errorHandler) {
			var config;

			if(typeof uidService.getUID() !== 'undefined') {
				config = {
					url: apiurl + 'comment/' + commentID + '/vote/' + choice,
					method: (!update) ? 'POST' : 'PUT',
					headers: {
						Authorization: uidService.getUID()
					}
				};
			} else {
				config = {
					url: apiurl + 'comment/' + commentID + '/vote/' + choice,
					method: (!update) ? 'POST' : 'PUT'
				};
			}

			$http(
				config
			).then(
				function successCallback(response) {
					if(typeof response.data.verification.uid !== 'undefined') {
						uidService.saveUID(response.data.verification.uid);
					}
					successHandler(response);
				},
				function errorCallback(response) {
					errorHandler(response);
				}
			);
		}

		function deleteCommentVote(commentID, successHandler, errorHandler) {
			var config;

			if(typeof uidService.getUID() !== 'undefined') {
				config = {
					url: apiurl + 'comment/' + commentID + '/vote',
					method: 'DELETE',
					headers: {
						Authorization: uidService.getUID()
					}
				};
			} else {
				config = {
					url: apiurl + 'comment/' + commentID + '/vote',
					method: 'DELETE'
				};
			}

			$http(
				config
			).then(
				function successCallback(response) {
					if(typeof response.data.verification.uid !== 'undefined') {
						uidService.saveUID(response.data.verification.uid);
					}
					successHandler(response);
				},
				function errorCallback(response) {
					errorHandler(response);
				}
			);
		}

		function remainingTime(postID, successHandler, errorHandler) {
			var config;

			if(typeof uidService.getUID() !== 'undefined') {
				config = {
					url: apiurl + 'user/time-remaining/post/' + postID + '/comment',
					method: 'GET',
					headers: {
						Authorization: uidService.getUID()
					}
				};
			} else {
				config = {
					url: apiurl + 'user/time-remaining/post/' + postID + '/comment',
					method: 'GET'
				};
			}

			$http(
				config
			).then(
				function successCallback(response) {
					if(typeof response.data.verification.uid !== 'undefined') {
						uidService.saveUID(response.data.verification.uid);
					}
					successHandler(response);
				},
				function errorCallback(response) {
					errorHandler(response);
				}
			);
		}

		function submit(postID, text, successHandler, errorHandler) {
			var config;

			if(typeof uidService.getUID() !== 'undefined') {
				config = {
					url: apiurl + 'post/' + postID + '/comment',
					method: 'POST',
					params: {text: text},
					headers: {
						Authorization: uidService.getUID()
					}
				};
			} else {
				config = {
					url: apiurl + 'post/' + postID + '/comment',
					method: 'POST',
					params: {text: text}
				};
			}

			$http(
				config
			).then(
				function successCallback(response) {

					if(response.data.verification.hasOwnProperty('uid')) {
						uidService.saveUID(response.data.verification.uid);
					}
					/*if(typeof response.data.verification.uid != 'undefined') {
						uidService.saveUID(response.data.verification.uid);
					}*/
					successHandler(response);
				},
				function errorCallback(response) {
					errorHandler(response);
				}
			);
		}
	};

	postCallsService.$inject = [
		'$window',
		'$http',
		'uidService',
		'apiurl'
	];

	/*global angular*/
	angular
		.module('izpovedi')
		.factory('postCallsService', postCallsService);
})();