(function() {
	'use strict';

	var postVoteService = function($http, uidService, apiurl) {
		var postVote = {
			vote: vote,
			deleteVote: deleteVote
		};

		return postVote;

		function vote(choice, postID, update, successHandler, errorHandler) {
			var config;

			if(typeof uidService.getUID() !== 'undefined') {
				config = {
					url: apiurl + 'post/' + postID + '/vote/' + choice,
					method: (!update) ? 'POST' : 'PUT',
					headers: {
						Authorization: uidService.getUID()
					}
				};
			} else {
				config = {
					url: apiurl + 'post/' + postID + '/vote/' + choice,
					method: (!update) ? 'POST' : 'PUT'
				};
			}

			$http(
				config
			).then(
				function successCallback(response) {
					if(typeof response.data.verification.uid !== 'undefined') {
						uidService.saveUID(response.data.verification.uid);
					}
					successHandler(response);
				},
				function errorCallback(response) {
					errorHandler(response);
				}
			);
		}

		function deleteVote(postID, successHandler, errorHandler) {
			var config;

			if(typeof uidService.getUID() !== 'undefined') {
				config = {
					url: apiurl + 'post/' + postID + '/vote',
					method: 'DELETE',
					headers: {
						Authorization: uidService.getUID()
					}
				};
			} else {
				config = {
					url: apiurl + 'post/' + postID + '/vote',
					method: 'DELETE'
				};
			}

			$http(
				config
			).then(
				function successCallback(response) {
					if(typeof response.data.verification.uid !== 'undefined') {
						uidService.saveUID(response.data.verification.uid);
					}
					successHandler(response);
				},
				function errorCallback(response) {
					errorHandler(response);
				}
			);
		}
	};

	postVoteService.$inject = [
		'$http',
		'uidService',
		'apiurl'
	];

	/*global angular*/
	angular
		.module('izpovedi')
		.factory('postVoteService', postVoteService);
})();