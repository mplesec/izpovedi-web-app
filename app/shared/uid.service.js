(function() {
	'use strict';

	var uidService = function($http, $window) {
		var uid = {
			saveUID: saveUID,
			getUID: getUID
		};

		return uid;

		function saveUID(uid) {
			$window.localStorage['izpovedi-uid'] = uid;
		}

		function getUID() {
			return $window.localStorage['izpovedi-uid'];
		}
	};

	uidService.$inject = [
		'$http',
		'$window'
	];

	/*global angular*/
	angular
		.module('izpovedi')
		.factory('uidService', uidService);
})();