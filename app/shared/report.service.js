(function() {
	'use strict';

	var reportService = function($http, uidService, apiurl) {
		var report = {
			reportPost: reportPost,
			deletePostReport: deletePostReport,
			reportComment: reportComment,
			deleteCommentReport: deleteCommentReport
		};

		return report;

		function reportPost(id,successHandler, errorHandler) {
			var config;

			if(typeof uidService.getUID() !== 'undefined') {
				config = {
					url: apiurl + 'post/' + id + '/report',
					method: 'POST',
					headers: {
						Authorization: uidService.getUID()
					}
				};
			} else {
				config = {
					url: apiurl + 'post/' + id + '/report',
					method: 'POST'
				};
			}

			$http(
				config
			).then(
				function successCallback(response) {
					if(typeof response.data.verification.uid !== 'undefined') {
						uidService.saveUID(response.data.verification.uid);
					}
					successHandler(response);
				},
				function errorCallback(response) {
					errorHandler(response);
				}
			);
		}

		function deletePostReport(id,successHandler, errorHandler) {
			var config;

			if(typeof uidService.getUID() !== 'undefined') {
				config = {
					url: apiurl + 'post/' + id + '/report',
					method: 'DELETE',
					headers: {
						Authorization: uidService.getUID()
					}
				};
			} else {
				config = {
					url: apiurl + 'post/' + id + '/report',
					method: 'DELETE'
				};
			}

			$http(
				config
			).then(
				function successCallback(response) {
					if(typeof response.data.verification.uid !== 'undefined') {
						uidService.saveUID(response.data.verification.uid);
					}
					successHandler(response);
				},
				function errorCallback(response) {
					errorHandler(response);
				}
			);
		}

		function reportComment(id,successHandler, errorHandler) {
			var config;

			if(typeof uidService.getUID() !== 'undefined') {
				config = {
					url: apiurl + 'comment/' + id + '/report',
					method: 'POST',
					headers: {
						Authorization: uidService.getUID()
					}
				};
			} else {
				config = {
					url: apiurl + 'comment/' + id + '/report',
					method: 'POST'
				};
			}

			$http(
				config
			).then(
				function successCallback(response) {
					if(typeof response.data.verification.uid !== 'undefined') {
						uidService.saveUID(response.data.verification.uid);
					}
					successHandler(response);
				},
				function errorCallback(response) {
					errorHandler(response);
				}
			);
		}

		function deleteCommentReport(id,successHandler, errorHandler) {
			var config;

			if(typeof uidService.getUID() !== 'undefined') {
				config = {
					url: apiurl + 'comment/' + id + '/report',
					method: 'DELETE',
					headers: {
						Authorization: uidService.getUID()
					}
				};
			} else {
				config = {
					url: apiurl + 'comment/' + id + '/report',
					method: 'DELETE'
				};
			}

			$http(
				config
			).then(
				function successCallback(response) {
					if(typeof response.data.verification.uid !== 'undefined') {
						uidService.saveUID(response.data.verification.uid);
					}
					successHandler(response);
				},
				function errorCallback(response) {
					errorHandler(response);
				}
			);
		}
	};

	reportService.$inject = [
		'$http',
		'uidService',
		'apiurl'
	];

	/*global angular*/
	angular
		.module('izpovedi')
		.factory('reportService', reportService);
})();