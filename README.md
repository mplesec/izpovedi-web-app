# izpovedi-app
Izpovedi front-end Angular app.

# how to run
Clone the repo and navigate to repo directory.
Run 'npm install', then 'bower install', then 'http-server'.

# how to generate new deploy
Run 'grunt generateDeploy' in top repo directory. You'll get a deploy folder with all the files needed for deploy. 

**If you add new files please update the copy task in Gruntfile.js.**

**If the task 'generateDeploy' doesn't seem to work try refreshing your directory or code editor or try deleting the deploy folder and run the task again.**